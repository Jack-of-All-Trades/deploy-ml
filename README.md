## Deploy ML based Chatbot

A simple chatbot application based on [chatterbot]("https://github.com/gunthercox/ChatterBot")

# Dependencies required/used

- Terraform (tested w/ version 0.12)
- Ansible (tested w/ version 2.8.0 and python version 3.7.3)

**please set the aws access key and secret key in terraform.tfvars file before running the steps below**

**Run following commands to provision the infrastructure**

1. ssh-keygen -f tfkey

2. terraform init

3. terraform apply --auto-approve

once these steps are completed. Terraform will output ec2 instance public ip. Please go to ip address using any browser of your choice to test the application.

**Run following command to destroy the infrastrucutre**

terraform destroy --auto-approve
