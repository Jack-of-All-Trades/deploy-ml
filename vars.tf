variable "AWS_REGION" {
  default = "ap-southeast-1"
}
variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}
variable "PRIVATE_KEY" {
  default = "tfkey"
}
variable "PUBLIC_KEY" {
  default = "tfkey.pub"
}
variable "AMIS" {
  type = "map"
  default = {
      ap-southeast-1 = "ami-0bab153cd7bd6fb18"
  }
}
variable "INSTANCE_TYPE" {
    default = "t2.small"
}

variable "INSTANCE_NAME" {
    default = "ML-prod"
}

variable "INSTANCE_DEVICE_NAME" {
  default = "/dev/xvdh"
}
