resource "aws_instance" "ML-prod" {
  ami           = "${lookup(var.AMIS, var.AWS_REGION)}"
  instance_type = "${var.INSTANCE_TYPE}"

  # the VPC subnet
  subnet_id = "${aws_subnet.main-public.id}"

  # the security group
  vpc_security_group_ids = ["${aws_security_group.allow-ssh.id}"]

  # the public SSH key
  key_name = "${aws_key_pair.awskeypair.key_name}"

  provisioner "local-exec" {
    command = "chmod +x ./script.sh; ./script.sh ${aws_instance.ML-prod.id} ${aws_instance.ML-prod.public_ip}"
  }
  connection {
    user        = "ubuntu"
    type        = "ssh"
    private_key = "${file("${var.PRIVATE_KEY}")}"
    agent       = "true"
    host        = "${aws_instance.ML-prod.public_ip}"
  }
}

# can be used to add additional ebs volume 
# resource "aws_ebs_volume" "ebs-volume-1" {
#     availability_zone = "ap-southeast-1a"
#     size = 20
#     type = "gp2" 
#     tags = {
#         Name = "extra volume data"
#     }
# }

# resource "aws_volume_attachment" "ebs-volume-1-attachment" {
#   device_name = "${var.INSTANCE_DEVICE_NAME}"
#   volume_id = "${aws_ebs_volume.ebs-volume-1.id}"
#   instance_id = "${aws_instance.ml_prod.id}"
# }

