#!/bin/bash

Hostfile=./provisioning/hosts
Privatekey=$(pwd)/tfkey

[ -f $Hostfile ] && rm $Hostfile

cd provisioning; echo -e "[prod]\n$1 ansible_host=$2 ansible_ssh_private_key_file=$Privatekey" >> hosts
ansible-playbook site.yml